<?php
/**
 * @file
 * Code for the UAQS Event feature.
 */

include_once 'uaqs_event.features.inc';

/**
 * Implements hook_theme_registry_alter().
 */
function uaqs_event_theme_registry_alter(&$theme_registry) {
  // Find all .tpl.php files in this module's folder recursively.
  $module_path = drupal_get_path('module', 'uaqs_event');
  $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path);

  // Iterate through all found template file objects.
  foreach ($template_file_objects as $key => $template_file_object) {
    // If the template has not already been overridden by a theme.
    if (!isset($theme_registry[$key]['theme path']) || !preg_match('#/themes/#', $theme_registry[$key]['theme path'])) {
      // Alter the theme path and template elements.
      $theme_registry[$key]['theme path'] = $module_path;
      $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
      $theme_registry[$key]['type'] = 'module';
    }
  }
}

/**
 * Implements hook_preprocess_node().
 */
function uaqs_event_preprocess_node(&$vars) {
  if ($vars['node']->type == 'uaqs_event' && $vars['view_mode'] == 'uaqs_card') {
    $vars['theme_hook_suggestions'][] = 'node__uaqs_event__uaqs_card';
  }
}

/**
 * Implements hook_field_display_alter().
 *
 * If events are in in a view using the node display row plugin, we need to help nodes
 * using the field_uaqs_event field to find the next date to display.
 *
 * What we are doing here is passing the value of the field from the
 * view which is force using fields, to the start from date 'multiple_from' on
 * the entity. This is mostly useful with repeating events.                                          /
 * @todo should add  a condition that the view forces using fields
 */
function uaqs_event_field_display_alter(&$display, $context) {
  if ($context['field']['field_name'] == 'field_uaqs_date' && isset($context['entity']->view) && $context['entity']->view->style_plugin->row_plugin->plugin_name == 'node') {
    $row_index = $context['entity']->view->row_index;
    $display['settings']['multiple_from'] = $context['entity']->view->result[$row_index]->field_data_field_uaqs_date_field_uaqs_date_value;
  }
}
